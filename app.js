
function send(){
    let msg = document.getElementById("message-input").value
    if(msg!==""){
        // Create the div
        var newDiv = document.createElement("div") 
        newDiv.classList.add("internal-message")

        // Set the username
        var userName = document.getElementById("userName").textContent
        userName = document.createTextNode(userName)
        var pUserName = document.createElement("p")
        pUserName.appendChild(userName)
        pUserName.classList.add("message-p")
        pUserName.classList.add("sender")
        newDiv.appendChild(pUserName)

        // Append message
        var newMsg = document.createTextNode(msg)
        newDiv.appendChild(newMsg)

        // Set the time
        var d = new Date()
        var timeStamp = document.createTextNode(d.getHours()+":"+d.getMinutes())
        var pTimestamp = document.createElement("p")
        pTimestamp.appendChild(timeStamp)
        pTimestamp.classList.add("message-p")
        pTimestamp.classList.add("timestamp")
        newDiv.appendChild(pTimestamp)

        // Display the message in the chat
        var messagesDisplay = document.getElementById("messages-display")
        messagesDisplay.appendChild(newDiv)
        document.getElementById("message-input").value = ""

        // Scroll down
        messagesDisplay.scrollTo(0,document.querySelector(".messages-display").scrollHeight);
        
        // Change nickname
        if(msg.split(" ")[0] == "/nickname"){
            nickChange(msg)
        }

        if(msg == "/joke"){
            askJoke()
        }
    }
}

// function receive(){
//     let msg = document.getElementById("message-input").value
//     if(msg!==""){
//         var newDiv = document.createElement("div")
//         newDiv.classList.add("external-message")
//         var newMsg = document.createTextNode(msg)
//         newDiv.appendChild(newMsg)
//         var messagesDisplay = document.getElementById("messages-display")
//         messagesDisplay.appendChild(newDiv)
//         document.getElementById("message-input").value = ""
//     }
// }

function receiveAuto(msg){
    var newDiv = document.createElement("div")
    newDiv.classList.add("external-message")

    // Set the username
    var userName = "Chatbot"
    userName = document.createTextNode(userName)
    var pUserName = document.createElement("p")
    pUserName.appendChild(userName)
    pUserName.classList.add("message-p")
    pUserName.classList.add("sender")
    newDiv.appendChild(pUserName)

    // Append message
    var newMsg = document.createTextNode(msg)
    newDiv.appendChild(newMsg)

    // Set the time
    var d = new Date()
    var timeStamp = document.createTextNode(d.getHours()+":"+d.getMinutes())
    var pTimestamp = document.createElement("p")
    pTimestamp.appendChild(timeStamp)
    pTimestamp.classList.add("message-p")
    pTimestamp.classList.add("timestamp")
    newDiv.appendChild(pTimestamp)

    // Display the message
    var messagesDisplay = document.getElementById("messages-display")
    messagesDisplay.appendChild(newDiv)

    // Scroll down
    messagesDisplay.scrollTo(0,document.querySelector(".messages-display").scrollHeight);
}

function nickChange(msg){
    nick = msg.split(" ").slice(1).join(" ")
    document.getElementById("userName").textContent = nick
    receiveAuto(" Your new nickname is now: "+nick)
}

function askJoke(){
    const xhr = new XMLHttpRequest()
    xhr.open('GET','http://api.icndb.com/jokes/random', true)
    xhr.onload = function(){
        if(this.status === 200){
            const response = JSON.parse(this.responseText)
            receiveAuto(response.value.joke.replace(/&quot;/g,'"'))
        }
    }
    xhr.send()
}